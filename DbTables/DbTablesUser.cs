﻿using Db.Sber.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Sber.DbTables
{
    internal class DbTablesUser
    {

        public static string? ConnectionString { get; set; }
        public static void Create(string sql)
        {
            try
            {
                using NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);
                connection.Open();

                using NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                //string affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                //Console.WriteLine(value: $"Table created. Affected rows count: {affectedRowsCount}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.ToString());
            }
        }

        public static void CreateUsers()
        {
            string sql = @"CREATE TABLE Users
            (
            Id                  SERIAL                 PRIMARY KEY,
            Name             CHARACTER VARYING(256)      NOT NULL,
            Email              CHARACTER VARYING(256)     NOT NULL UNIQUE,
            PhoneNumber         CHARACTER VARYING(256)      NOT NULL);";

            Create(sql);
        }

        public static void FillingUsers()
        {
            User[] users = new User[5];

            users[0] = new User { Name = "Андрей", Email = "andrey@mail.ru", PhoneNumber = "+70000000001" };
            users[1] = new User { Name = "Борис", Email = "boris@mail.ru", PhoneNumber = "+70000000002" };
            users[2] = new User { Name = "Василии", Email = "vasya@mail.ru", PhoneNumber = "+70000000003" };
            users[3] = new User { Name = "Гриша", Email = "grisha@mail.ru", PhoneNumber = "+70000000004" };
            users[4] = new User { Name = "Диана", Email = "diana@mail.ru", PhoneNumber = "+70000000005" };

            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                try
                {
                    foreach (var user in users)
                        db.Users.Add(user);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public static void ReadUsers()
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                var users = db.Users.ToList();
                Console.WriteLine("\n");
                foreach (User u in users)

                    Console.WriteLine($"{u.Id}.{u.Name} - {u.Email} - {u.PhoneNumber}");
            }
        }

        static void AddUser(User user)
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }
        public static void AddUser()
        {
            try
            {
                User user = new User();

                Console.Write("Введите \"Name\": ");
                user.Name = Console.ReadLine();

                Console.Write("Введите \"Email\": ");
                user.Email = Console.ReadLine();

                Console.Write("Введите \"PhoneNumber\": ");
                user.PhoneNumber = Console.ReadLine();

                AddUser(user);

                Console.WriteLine($"Пользователь \"{user.Name}\" добавлен(а) в таблицу \"Users\"!\r\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
