﻿using Db.Sber.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Sber.DbTables
{
    internal class DbTablesPosition
    {
        public static string? ConnectionString { get; set; }
        public static void Create(string sql)
        {
            try
            {
                using NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);
                connection.Open();

                using NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                //string affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                //Console.WriteLine(value: $"Table created. Affected rows count: {affectedRowsCount}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.ToString());
            }
        }

        public static void CreatePositions()
        {
            string sql = @"CREATE TABLE Location(
                        Id                  SERIAL                 PRIMARY KEY,
                        NameDepartment             CHARACTER VARYING(256)      NOT NULL UNIQUE,
                        NamePosition              CHARACTER VARYING(256)     NOT NULL,
                        UserId         INTEGER REFERENCES Users (Id));";

            Create(sql);
        }

        public static void FillingPosition()
        {
            Position[] positions = new Position[5];

            positions[0] = new Position { NameDepartment = "Финансы", NamePosition = "финансист", UserId = 1 };
            positions[1] = new Position { NameDepartment = "Бухгалтерия", NamePosition = "бухгалтер", UserId = 2 };
            positions[2] = new Position { NameDepartment = "IT", NamePosition = "программист", UserId = 3 };
            positions[3] = new Position { NameDepartment = "Кредитный", NamePosition = "кредитный специалист", UserId = 4 };
            positions[4] = new Position { NameDepartment = "Охрана", NamePosition = "охранник", UserId = 5 };

            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                try
                {
                    foreach (var position in positions)
                        db.Positions.Add(position);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public static void ReadPositions()
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                var positions = db.Positions.ToList();
                Console.WriteLine("\n");
                foreach (Position p in positions)

                    Console.WriteLine($"{p.Id}.{p.NameDepartment} - {p.NamePosition} - {p.UserId}");
            }
        }

        static void AddPosition(Position position)
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                db.Positions.Add(position);
                db.SaveChanges();
            }
        }
        public static void AddPosition()
        {
            try
            {
                Position position = new Position();

                Console.Write("Введите \"NameDepartment\": ");
                position.NameDepartment = Console.ReadLine();

                Console.Write("Введите \"NamePosition\": ");
                position.NamePosition = Console.ReadLine();

                Console.Write("Введите \"UserId\": ");
                position.UserId = Convert.ToInt32(Console.ReadLine());

                AddPosition(position);

                Console.WriteLine($"Позиция \"{position.NameDepartment}\" добавлен(а) в таблицу \"Position\"!\r\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
