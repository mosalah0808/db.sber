﻿using Db.Sber.Entity;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Sber.DbTables
{
    internal class DbTablesLocation
    {
        public static string? ConnectionString { get; set; }
        public static void Create(string sql)
        {
            try
            {
                using NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);
                connection.Open();

                using NpgsqlCommand cmd = new NpgsqlCommand(sql, connection);
                //string affectedRowsCount = cmd.ExecuteNonQuery().ToString();
                //Console.WriteLine(value: $"Table created. Affected rows count: {affectedRowsCount}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка: " + ex.ToString());
            }
        }

        public static void CreateLocations()
        {
            string sql = @"CREATE TABLE Location(
                        Id                  SERIAL                 PRIMARY KEY,
                        Name             CHARACTER VARYING(256)      NOT NULL UNIQUE,
                        Address              CHARACTER VARYING(256)     NOT NULL,
                        UserId         INTEGER REFERENCES Users (Id));";

            Create(sql);
        }

        public static void FillingLocation()
        {
            Location[] locations = new Location[5];

            locations[0] = new Location { Name = "Архангельск", Address = "ул-ца Ленина 10", UserId = 1 };
            locations[1] = new Location { Name = "Борисов", Address = "ул-ца Ленина 20", UserId = 2 };
            locations[2] = new Location { Name = "Васюки", Address = "ул-ца Ленина 40", UserId = 3 };
            locations[3] = new Location { Name = "Гродно", Address = "ул-ца Ленина 50", UserId = 4 };
            locations[4] = new Location { Name = "Донецк", Address = "ул-ца Ленина 60", UserId = 5 };

            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                try
                {
                    foreach (var location in locations)
                        db.Locations.Add(location);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public static void ReadLocations()
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                var locations = db.Locations.ToList();
                Console.WriteLine("\n");
                foreach (Location l in locations)

                    Console.WriteLine($"{l.Id}.{l.Name} - {l.Address} - {l.UserId}");
            }
        }

        static void AddLocation(Location location)
        {
            using (ApplicationContext db = new ApplicationContext(ConnectionString))
            {
                db.Locations.Add(location);
                db.SaveChanges();
            }
        }
        public static void AddLocation()
        {
            try
            {
                Location location = new Location();

                Console.Write("Введите \"Name\": ");
                location.Name = Console.ReadLine();

                Console.Write("Введите \"Address\": ");
                location.Address = Console.ReadLine();

                Console.Write("Введите \"UserId\": ");
                location.UserId = Convert.ToInt32(Console.ReadLine());

                AddLocation(location);

                Console.WriteLine($"Локация \"{location.Name}\" добавлен(а) в таблицу \"Location\"!\r\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
