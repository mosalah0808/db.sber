﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Sber.Entity
{
    public class Position
    {
        public long Id { get; set; }
        public string? NameDepartment { get; set; }
        public string? NamePosition { get; set; }
        public int UserId { get; set; }

    }
}
