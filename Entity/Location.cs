﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Sber.Entity
{
    public class Location
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Address { get; set; }
        public int UserId { get; set; }    
        
    }
}
