﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db.Sber.DbTables;

namespace Db.Sber
{
    internal class Program
    {

        public static string CONNECTIONSTRING = "Host=localhost;Port=5432;Username=postgres;Password=123456;Database=sber;";
        static void Main(string[] args)
        {
            DbTablesUser.ConnectionString = CONNECTIONSTRING;
            DbTablesLocation.ConnectionString = CONNECTIONSTRING;
            DbTablesPosition.ConnectionString = CONNECTIONSTRING;

            
            DbTablesUser.CreateUsers();

            DbTablesUser.FillingUsers();

            DbTablesLocation.CreateLocations();

            DbTablesLocation.FillingLocation();

            DbTablesPosition.CreatePositions();

            DbTablesPosition.FillingPosition();



            ConsoleKey consoleKey;

            Console.WriteLine("------------ База данных PostgreSQL СБЕРБАНК ----------");

            while (true)
            {
                Console.WriteLine("==============================");
                Console.WriteLine("1 - Вывести все таблицы");
                Console.WriteLine("2 - Добавить запись в таблицу Users");
                Console.WriteLine("3 - Добавить запись в таблицу Location");
                Console.WriteLine("4 - Добавить запись в таблицу Position");
                Console.WriteLine("5 - Завершить работу");
                Console.WriteLine("==============================");

                consoleKey = Console.ReadKey(true).Key;

                if (consoleKey == ConsoleKey.D1)
                    DbTablesUser.ReadUsers();
                    DbTablesLocation.ReadLocations();
                    DbTablesPosition.ReadPositions();

                if (consoleKey == ConsoleKey.D2)
                    DbTablesUser.AddUser();

                if (consoleKey == ConsoleKey.D3)
                    DbTablesLocation.AddLocation();

                if (consoleKey == ConsoleKey.D4)
                    DbTablesPosition.AddPosition();
                
                if (consoleKey == ConsoleKey.D5)
                    return;
            }


        }
    }
}
